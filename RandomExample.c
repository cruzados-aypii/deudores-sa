#include "Lista.h"
#include "FileManager.h"
#include "RandomExample.h"
#include <time.h>
#include <string.h>

void generateExample(int sampleSize, char route[150]) {
	ListC list = NULL;
	struct NodeC *tempNodeC;
	struct NodeD *tempNodeD;
	int tempInt;
	if (sampleSize < 10) {
		printf("Sample size too small to be useful.");
		return;
	} else if (sampleSize > 10000) {
		printf("Sample size too large to be functional.");
		return;
	}
	srand(time(0));

	for (int i = 1; i <= sampleSize; i++) {
		insertClient(&list, randClient());
		if (i % 100 == 0) {	//save every 100 clients

			adjustRecommendations(list, randInt(10, 33), 100);
			tempNodeC = list;
			while (tempNodeC != NULL) {
				saveClient(tempNodeC->value, route);
				tempNodeD = tempNodeC->value->debts;
				while (tempNodeD != NULL) {
					saveDebt(tempNodeD->value, route);
					tempNodeD = tempNodeD->next;
				}
				tempNodeC = tempNodeC->next;
			}
			list = NULL;

		} else if (i == sampleSize) { //for the last batch of clients, which don't add up to 100
			tempInt = i % 100;
			adjustRecommendations(list, randInt(tempInt/10, tempInt/3), tempInt);
			tempNodeC = list;
			while (tempNodeC != NULL) {
				saveClient(tempNodeC->value, route);
				tempNodeD = tempNodeC->value->debts;
				while (tempNodeD != NULL) {
					saveDebt(tempNodeD->value, route);
					tempNodeD = tempNodeD->next;
				}
				tempNodeC = tempNodeC->next;
			}
		}
	}
}

Client randClient() {
	char charPool[] = "abcdefghijkmlnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
	char name[41];
	char surname[43];
	memset(name, '\0', sizeof(name));
	memset(surname, '\0', sizeof(name));
	int dni;
	int age;
	Client client;

	dni = randInt(40000000, 49999999);
	age = randInt(18, 80);
	randString(name, 3, 40, charPool, 53);
	if (name[0] >= 'a') {
		name[0] -= 32;
	}
	randString(surname, 4, 42, charPool, 53);
	
	client = initializeClient(name, surname, dni, age, 0, NULL);
	randDebts(client);
	return client;
}

void randDebts(Client client) {
	int id;
	char emmissionDate[11];
	char expirationDate[11];
	int numberOfFees;
	int remainingFees;
	int requestedAmount;
	int amountDue;
	memset(emmissionDate, '\0', sizeof(emmissionDate));
	memset(expirationDate, '\0', sizeof(expirationDate));

	for (int i = randInt(1, 3); i > 0; i--) {
		id = randInt(10000000, 99999999);
		randDate(emmissionDate, 1998, 2001);
		numberOfFees = randInt(1, 4);
		switch(numberOfFees) {
		case 1:
			numberOfFees = 6;
			break;
		case 2:
			numberOfFees = 12;
			break;
		case 3:
			numberOfFees = 24;
			break;
		case 4:
			numberOfFees = 36;
			break;
		default:
			break;
		}

		addDate(expirationDate, emmissionDate, numberOfFees);
		remainingFees = randInt(1, numberOfFees);
		requestedAmount = randInt(10000, 500000);
		amountDue = requestedAmount/numberOfFees*remainingFees;
		insertDebt(&(client->debts), initializeDebt(id, client->dni, emmissionDate, expirationDate, numberOfFees, remainingFees, requestedAmount, amountDue));
	}
}

void adjustRecommendations(ListC clients, int numberOfReferences, int numberOfClients) {
	int clientPos;
	int reference;
	struct NodeC *tempNode;
	for (int i = 0; i < numberOfReferences; i++) {
		tempNode = clients;
		clientPos = randInt(1, numberOfClients);
		for (int j = 0; j < clientPos; j++) {
			tempNode = tempNode->next;
		}
		reference = tempNode->value->dni;

		clientPos = randInt(1, numberOfClients);
		tempNode = clients;
		for (int j = 0; j < clientPos; j++) {
			tempNode = tempNode->next;
		}
		tempNode->value->dniRecommendedBy = reference;
	}
}

void randDate(char date[11], int lowerYear, int upperYear) {
	int year = randInt(lowerYear, upperYear);
	int month = randInt(1, 12);
	int day;
	switch(month) {
	case 2:
		if (year == 2000) {
			day = randInt(1, 29);
		} else {
			day = randInt(1, 28);
		}
		break;
	case 4:
		day = randInt(1, 30);
		break;
	case 6:
		day = randInt(1, 30);
		break;
	case 9:
		day = randInt(1, 30);
		break;
	case 11:
		day = randInt(1, 30);
		break;
	default:
		day = randInt(1, 31);
		break;
	}
	sprintf(date, "%d-%d-%d", day, month, year);
}

void addDate(char expirationDate[11], char emmissionDate[11], int numberOfFees) {
	int year;
	int month;
	int day;

	sscanf(emmissionDate, "%d-%d-%d", &day, &month, &year);
	month += numberOfFees;

	if (month > 12) {
		if (month > 24) {
			if (month > 36) {
				month -= 36;
				year += 3;
			} else {
				month -= 24;
				year += 2;
			}
		} else {
			month -= 12;
			year += 1;
		}
	}

	sprintf(expirationDate, "%d-%d-%d", day, month, year);
}

int randInt(int lowerBound, int upperBound) {
	if (lowerBound < RAND_MAX && upperBound < RAND_MAX) {
		return rand() % (upperBound - lowerBound) + lowerBound;
	} else {
		return (rand()*(RAND_MAX+1)+rand()) % (upperBound - lowerBound) + lowerBound;
	}
}

void randString(char buffer[], int minSize, int maxSize, char charPool[], int poolSize) {
	int temp;
	int j = randInt(minSize-1, maxSize-1);
	buffer[j+1] = '\0';
	while (j >= 0) {
		temp = rand() % (poolSize-1);
		buffer[j] = charPool[temp];
		j--;
	}
}