#ifndef FILEMANAGER_H
#define FILEMANAGER_H
#include "Lista.h"
#include <stdio.h>
#include <stdlib.h>
#ifndef NULL
#define NULL ((void*)0)
#endif
#define TRUE 1
#define FALSE 0

void loadAndPrintClients(char route[150], int inPages);
Client loadClientDNI(int dni, char route[150]);
Client loadClientName(char name[41], char surname[43], char route[150]);
Debt loadDebt(int id, char fullRoute[162]);
void loadAndPrintRangeOfClientsAge(int lowerAgeBound, int upperAgeBound, char route[150]);
void getClientNameInOrder(char name[41], int count,char route[150]);
void writeClientInFile(Client client, FILE *clientFile);
void saveClient(Client client,char route[150]);
void saveDebt(Debt debt,char route[150]);
int deleteDebt(int id, char route[150]);
void editDebt(int id, int newAmount, int newFees, char route[150]);
int addIdCreditToClient(int dniClient, int id, char route[150]);
int deleteIdInClientFile(int dni,int id, char route[150]);

Debt initializeDebt(int id, int dniDebtor, char emmissionDate[18], char expirationDate[18], int numberOfFees, int feesRemaining, int amount, int remainingAmount);
int strtoint(char integer[], int last_pos);
int power(int base, int exponent);
Client initializeClient(char name[41], char surname[43], int dni, int age, int dniRecommendedBy, ListD debts);
void skipLine(FILE *file);
void cleanStrings(char str1[], int size1, char str2[], int size2, char str3[], int size3);
void appendChar(char buffer[], char toAppend);
int namecmp(char str1[], char str2[], int length1, int length2);
int requestConfirmation(char current, int inPages);
void modifyDebt(int newAmount, int newFees, FILE *debtFile, char line[250]);
char nextSemiColon(FILE *file);

#endif
