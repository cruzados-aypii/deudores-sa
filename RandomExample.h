#ifndef RANDOMEXAMPLE_H
#define RANDOMEXAMPLE_H
#include "Lista.h"
#include "FileManager.h"
#include <stdio.h>
#include <stdlib.h>

void generateExample(int sampleSize, char route[150]);
Client randClient();
void randDebts(Client client);
void adjustRecommendations(ListC clients, int numberOfReferences, int numberOfClients);
void randDate(char date[10], int lowerYear, int upperYear);
void addDate(char emmissionDate[10], char expirationDate[10], int numberOfFees);
int randInt(int lowerBound, int upperBound);
void randString(char buffer[], int minSize, int maxSize, char charPool[], int poolSize);

#endif
