#include "FileManager.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Lista.h"

/*Function which loads a list of 100 bank clients stored in csv format at route\\clients.txt,
prints it, and loads another 100. Keeps printing until save file is fully read. Save file is
assumed to be sorted by client name.*/
void loadAndPrintClients(char route[150], int inPages) {
	char fullRoute[162];
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
    FILE *clientFile = fopen(fullRoute, "r");
    char current = fgetc(clientFile);
    ListC list;
	list = NULL;
    char name[41];
    char surname[43];
    int age;
    int dni;
    int recommendedBy;
	ListD debts;
	debts = NULL;
    int pos = 0;		//position in the current line of the csv file
    int index = 0;		//index for char[] variables
    char temp_int[9];	//variable for temporary storage of extracted numbers
	int loadedClients = 0;	//keeps count of how many clients were loaded

	cleanStrings(name, sizeof(name), surname, sizeof(surname), temp_int, sizeof(temp_int));
	memset(fullRoute, '\0', sizeof(fullRoute));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\debts.txt");

	while (current != EOF) {
		while (loadedClients < 100 && current != EOF) {
			while (current != ';') {
				switch(pos) {
				case 0:		//name
					name[index] = current;
					break;
				case 1:		//surname
					surname[index] = current;
					break;
				default:	//dni, age, din of recommender, debt id
					temp_int[index] = current;
					break;
				}
				index++;
				current = fgetc(clientFile);
			}

			switch(pos) {	//Case 0 and 1 are there to allow the use of default case for
							//loading several debts without errors, as these first cases
							//should be skipped.
			case 0:
				break;
			case 1:
				break;
			case 2:
				dni = strtoint(temp_int, 7);
				break;
			case 3:
				age = strtoint(temp_int, 1);
				break;
			case 4:
				if (strcmp(temp_int, "0") != 0) {
					recommendedBy = strtoint(temp_int, 7);
				} else {
					recommendedBy = 0;
				}
				break;
			default:	//Code allows for the loading of an arbitrary number of debts.
						//In practice, the amount of debts allowed per client is 3.
				insertDebt(&debts, loadDebt(strtoint(temp_int, 7), fullRoute));
				break;
			}
			current = fgetc(clientFile);
			index = 0;

			if (current == '\n') {	//Reached end of the current line
				insertClient(&list, initializeClient(name, surname, dni, age, recommendedBy, debts));
				cleanStrings(name, sizeof(name), surname, sizeof(surname), temp_int, sizeof(temp_int));
				pos = 0;
				debts = NULL;
				loadedClients++;
				current = fgetc(clientFile);
			} else {
				memset(temp_int, '\0', sizeof(temp_int));
				pos++;
			}
		}

		printListC(list, route);
		if (!requestConfirmation(current, inPages)) {
			break;
		}
		loadedClients = 0;
		list = NULL;
	}
	fclose(clientFile);
}

/*Function which loads and returns a client with a specific DNI, stored in CSV format at
route\\clients.txt*/
Client loadClientDNI(int dni, char route[150]) {
	Client client = malloc(sizeof(struct Client));
	client->debts = NULL;
	char fullRoute[162];
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
	FILE *clientFile = fopen(fullRoute, "r");
	int pos = 0;
	int index = 0;
	char temp_int[9];
	char current = fgetc(clientFile);
	int found = 0;
	memset(fullRoute, '\0', sizeof(fullRoute));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\debts.txt");

	cleanStrings(client->name, sizeof(client->name), client->surname, sizeof(client->surname), temp_int, sizeof(temp_int));

	while (current != EOF) {
		while (current != ';' && current != '\n') {
			switch(pos) {
			case 0:		//name
				client->name[index] = current;
				break;
			case 1:		//surname
				client->surname[index] = current;
				break;
			default:	//dni, age, dni of recommender, debt id
				temp_int[index] = current;
				break;
			}
			index++;
			current = fgetc(clientFile);
		}

		switch(pos) {	//Case 0 and 1 are there to allow the use of default case for
						//loading several debts without errors, as these first cases
						//should be skipped.
		case 0:
			break;
		case 1:
			break;
		case 2:
			client->dni = strtoint(temp_int, 7);
			if (dni == client->dni) {
				found = 1;
			} else {	//dni doesn't match
				skipLine(clientFile);
				cleanStrings(client->name, sizeof(client->name), client->surname, sizeof(client->surname), temp_int, sizeof(temp_int));
				pos = 0;
				index = 0;
				current = fgetc(clientFile);
				continue;
			}
			break;
		case 3:
			client->age = strtoint(temp_int, 1);
			break;
		case 4:
			client->dniRecommendedBy = strtoint(temp_int, strlen(temp_int)-1);
			break;
		default:	//Code allows for the loading of an arbitrary number of debts.
					//In practice, the amount of debts allowed per client is 3.
			insertDebt(&(client->debts), loadDebt(strtoint(temp_int, 7), fullRoute));
			break;
		}

		current = fgetc(clientFile);
		index = 0;

		if (current == '\n') {	//Reached end of the current line
			if (found == 1) {
				break;
			}
			cleanStrings(client->name, sizeof(client->name), client->surname, sizeof(client->surname), temp_int, sizeof(temp_int));
			pos = 0;
			current = fgetc(clientFile);
		} else {
			pos++;
			memset(temp_int, '\0', sizeof(temp_int));
		}
	}

	if (found == 0) {
		client = NULL;
	}
	fclose(clientFile);
	return client;
}

/*Function which loads and returns a client with a specific name and surname, stored in CSV
format at route\\clients.txt*/
Client loadClientName(char name[41], char surname[43], char route[150]) {
	Client client = malloc(sizeof(struct Client));
	client->debts = NULL;
	char fullRoute[162];
	memset(fullRoute, '\0', sizeof(fullRoute));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
	FILE *clientFile = fopen(fullRoute, "r");
	int pos = 0;
	int index = 0;
	char temp_int[9];
	char current = fgetc(clientFile);
	int found = 0;
	memset(fullRoute, '\0', sizeof(fullRoute));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\debts.txt");

	cleanStrings(client->name, sizeof(client->name), client->surname, sizeof(client->surname), temp_int, sizeof(temp_int));

	while (current != EOF) {
		while (current != ';') {
			switch(pos) {
			case 0:		//name
				client->name[index] = current;
				break;
			case 1:		//surname
				client->surname[index] = current;
				break;
			default:	//dni, age, dni of recommender, debt id
				temp_int[index] = current;
				break;
			}
			index++;
			current = fgetc(clientFile);
		}
		switch(pos) {
		case 0:
			if (namecmp(name, client->name, 41, 41) > 0) {	//name doesn't match, but client may still be available
				skipLine(clientFile);
				cleanStrings(client->name, sizeof(client->name), client->surname, sizeof(client->surname), temp_int, sizeof(temp_int));
				index = 0;
				current = fgetc(clientFile);
				continue;
			} else if (namecmp(name, client->name, 41, 41) < 0) {	//client isn't available
				current = EOF;
				continue;
			}
			break;
		case 1:
			if (strcmp(surname, client->surname) != 0) {	//surname doesn't match
				skipLine(clientFile);
				cleanStrings(client->name, sizeof(client->name), client->surname, sizeof(client->surname), temp_int, sizeof(temp_int));
				pos = 0;
				index = 0;
				current = fgetc(clientFile);
				continue;
			}
			found = 1;
			break;
		case 2:
			client->dni = strtoint(temp_int, 7);
			break;
		case 3:
			client->age = strtoint(temp_int, 1);
			break;
		case 4:
			client->dniRecommendedBy = strtoint(temp_int, strlen(temp_int)-1);
			break;

		default:	//Code allows for the loading of an arbitrary number of debts.
					//In practice, the amount of debts allowed per client is 3.
			insertDebt(&(client->debts), loadDebt(strtoint(temp_int, 7), fullRoute));
			break;
		}

		current = fgetc(clientFile);
		index = 0;

		if (current == '\n') {	//Reached end of the current line
			if (found == 1) {
				break;
			}
			cleanStrings(client->name, sizeof(client->name), client->surname, sizeof(client->surname), temp_int, sizeof(temp_int));
			pos = 0;
			current = fgetc(clientFile);
		} else {
			pos++;
			memset(temp_int, '\0', sizeof(temp_int));
		}
	}

	if (found == 0) { //client wasn't found
		client = NULL;
	}
	fclose(clientFile);
	return client;
}

/*Function which loads and returns a debt with a specific ID, stored in CSV format at
fullRoute. This function is meant to be called by client loading functions which
already have the directory address.*/
Debt loadDebt(int id, char fullRoute[162]) {
	Debt debt = malloc(sizeof(struct Debt));
	char temp_int[9];
	int pos = 0;
	int index = 0;
	int found = 0;
	FILE *debtFile = fopen(fullRoute, "r");
	char current = fgetc(debtFile);
	cleanStrings(debt->emmissionDate, sizeof(debt->emmissionDate), debt->expirationDate, sizeof(debt->expirationDate), temp_int, sizeof(temp_int));

	while (current != EOF) {
		while (current != ';') {
			switch(pos) {
			case 2:
				debt->emmissionDate[index] = current;
				break;
			case 3:
				debt->expirationDate[index] = current;
				break;
			default:
				temp_int[index] = current;
				break;
			}
			index++;
			current = fgetc(debtFile);
		}

		switch(pos) {
		case 0:
			debt->id = strtoint(temp_int, 7);
			if (debt->id == id) {
				found = 1;
			} else {
				skipLine(debtFile);
				index = 0;
				pos = 0;
				current = fgetc(debtFile);
				memset(temp_int, '\0', sizeof(temp_int));
				continue;
			}
			break;
		case 1:
			debt->dniDebtor = strtoint(temp_int, 7);
			break;
		case 4:
			debt->numberOfFees = strtoint(temp_int, strlen(temp_int)-1);
			break;
		case 5:
			debt->feesRemaining = strtoint(temp_int, strlen(temp_int)-1);
			break;
		case 6:
			debt->amount = strtoint(temp_int, strlen(temp_int)-1);
			break;
		case 7:
			debt->remainingAmount = strtoint(temp_int, strlen(temp_int)-1);
			break;
		default:
			break;
		}

		current = fgetc(debtFile);
		index = 0;
		if (current == '\n') {	//Reached end of the current line
			if (found == 1) {
				break;
			}
			cleanStrings(debt->emmissionDate, sizeof(debt->emmissionDate), debt->expirationDate, sizeof(debt->expirationDate), temp_int, sizeof(temp_int));
			pos = 0;
			current = fgetc(debtFile);
		} else {
			pos++;
			memset(temp_int, '\0', sizeof(temp_int));
		}
	}
	fclose(debtFile);
	if (found == 0) {
		debt = NULL;
	}
	return debt;
}

/*Function which loads a list of clients with an age higher or equal to lowerAgeBound
and lower or equal to upperAgeBound, stored at route\\clients.txt. 100 clients will
be loaded (if available), printed, and then another 100.*/
void loadAndPrintRangeOfClientsAge(int lowerAgeBound, int upperAgeBound, char route[150]) {
	char fullRoute[162];
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
    FILE *clientFile = fopen(fullRoute, "r");
    char current = fgetc(clientFile);
    ListC list;
	list = NULL;
    int pos = 0;		//position in the current line of the csv file
    int index = 0;		//index for char[] variables
    char temp_int[9];	//variable for temporary storage of extracted numbers
	int loadedClients = 0;	//keeps count of how many clients were loaded

    char name[41];
    char surname[43];
    int age;
    int dni;
    int recommendedBy;
	ListD debts;
	debts = NULL;

	memset(fullRoute, '\0', sizeof(fullRoute));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\debts.txt");
	cleanStrings(name, sizeof(name), surname, sizeof(surname), temp_int, sizeof(temp_int));

	while (current != EOF) {
		while (loadedClients < 100 && current != EOF) {
			while (current != ';') {
				switch(pos) {
				case 0:		//name
					name[index] = current;
					break;
				case 1:		//surname
					surname[index] = current;
					break;
				default:	//dni, age, dni of recommender, debt id
					temp_int[index] = current;
					break;
				}
				index++;
				current = fgetc(clientFile);
			}

			switch(pos) {	//Case 0 and 1 are there to allow the use of default case for
							//loading several debts without errors, as these first cases
							//should be skipped.
			case 0:
				break;
			case 1:
				break;
			case 2:
				dni = strtoint(temp_int, 7);
				break;
			case 3:
				age = strtoint(temp_int, 1);
				if (age < lowerAgeBound || age > upperAgeBound) {	//client doesn't fit the range, moving on to next line
					cleanStrings(name, sizeof(name), surname, sizeof(surname), temp_int, sizeof(temp_int));
					skipLine(clientFile);
					index = 0;
					pos = 0;
					current = fgetc(clientFile);
					continue;
				}
				break;
			case 4:
				recommendedBy = strtoint(temp_int, strlen(temp_int)-1);
				break;
			default:	//Code allows for the loading of an arbitrary number of debts.
						//In practice, the amount of debts allowed per client is 3.
				insertDebt(&debts, loadDebt(strtoint(temp_int, 7), fullRoute));
				break;
			}
			current = fgetc(clientFile);
			index = 0;

			if (current == '\n') {	//Reached end of the current line
				insertClient(&list, initializeClient(name, surname, dni, age, recommendedBy, debts));
				cleanStrings(name, sizeof(name), surname, sizeof(surname), temp_int, sizeof(temp_int));
				pos = 0;
				debts = NULL;
				loadedClients++;
			} else {
				memset(temp_int, '\0', sizeof(temp_int));
				pos++;
			}
		}

		printListC(list, route);
		if (!requestConfirmation(current, TRUE)) {
			break;
		}
		loadedClients = 0;
		list = NULL;
	}
	fclose(clientFile);
}

/*Function which initializes a debt based on the data given.*/
Debt initializeDebt(int id, int dniDebtor, char emmissionDate[11], char expirationDate[11], int numberOfFees, int feesRemaining, int amount, int remainingAmount) {
	Debt debt = malloc(sizeof(struct Debt));
	debt->id = id;
	debt->dniDebtor = dniDebtor;
	strcpy(debt->emmissionDate, emmissionDate);
	strcpy(debt->expirationDate, expirationDate);
	debt->numberOfFees = numberOfFees;
	debt->feesRemaining = feesRemaining;
	debt->amount = amount;
	debt->remainingAmount = remainingAmount;
	return debt;
}

/*Auxiliary function which converts an integer from char[] format to int.*/
int strtoint(char integer[], int last_pos) {
    int ret = 0;
    int j = 0;

    for (int i = last_pos; i >= 0; i--) {
        ret += (integer[j] - '0')*power(10, i);
		j++;
    }

	return ret;
}

/*Auxiliary function which calculates a base integer raised to the power of exponent.
Does not support negative or fraction exponent.*/
int power(int base, int exponent) {
    int ret = base;

	if (exponent == 0) {
		return 1;
	}

    for (int i = 1; i < exponent; i++) {
        ret = ret*base;
    }

    return ret;
}

/*Auxiliary function which initializes a client based on the data given.*/
Client initializeClient(char name[41], char surname[43], int dni, int age, int dniRecommendedBy, ListD debts) {
	Client client = malloc(sizeof(struct Client));
	strcpy(client->name, name);
	strcpy(client->surname, surname);
	client->dni = dni;
	client->age = age;
	client->dniRecommendedBy = dniRecommendedBy;	//Information about the recommender should be requested later
												//by the user, with a separate function
	client->debts = debts;
	return client;
}

/*Auxiliary function which skips an unnecessary line. Next fget call will
return characters from the next line.*/
void skipLine(FILE *file) {
	char current = fgetc(file);
	while(current != '\n') {
		current = fgetc(file);
	}
}

/*Auxiliary function which fills three arbitrarily sized char arrays with null
characters. Meant to reinitialize variables in a loop for load functions.*/
void cleanStrings(char str1[], int size1, char str2[], int size2, char str3[], int size3) {
	memset(str1, '\0', size1);
	memset(str2, '\0', size2);
	memset(str3, '\0', size3);
}

/*Auxiliary function which appends a char to the end of a string. The string
must be null (\\0) terminated.*/
void appendChar(char buffer[], char toAppend) {
	int pos = 0;
	
	while(buffer[pos] != '\0') {
		pos++;
	}
	buffer[pos] = toAppend;
	buffer[pos+1] = '\0';
}

/*Auxiliary function which compares str1 to str2. Returns a negative integer,
zero or a positive integer if str1 is less than, equal to or more than str2.
It's different from strcmp in that it doesn't differentiate between capital
and small case when comparing. Only works with non-special characers.*/
int namecmp(char str1[], char str2[], int length1, int length2) {
	char temp1[length1];
	char temp2[length2];
	char current;
	int index = 0;
	strcpy(temp1, str1);
	strcpy(temp2, str2);
	current = temp1[0];
	
	while (current != '\0') {
		if (current >= 'a') {
			temp1[index] -= 32;
		}
		index++;
		current = temp1[index];
	}

	index = 0;
	current = temp2[0];

	while (current != '\0') {
		if (current >= 'a') {
			temp2[index] -= 32;
		}
		index++;
		current = temp2[index];
	}

	return strcmp(temp1, temp2);
}

/*Auxiliary function which requests the user for confirmation to continue printing
clients. Employed by loadAndPrintClients and loadAndPrintRangeOfClientsAge.*/
int requestConfirmation(char current, int inPages) {
	char YoN;
	int exit = FALSE;
	int confirmation;

	if (!inPages || current == EOF) {
		confirmation = exit = TRUE;
	}
	printf("Para mas informacion sobre los creditos de un cliente, busque al cliente correspondiente por su DNI o nombre.\n\n");

	while (!exit) {
		exit = TRUE;
		printf("Desea cargar mas clientes? Y-N\n");
		scanf("%c", &YoN);
		while ((getchar()) != '\n');
		if (YoN == 'Y' || YoN == 'y') {
			confirmation = TRUE;
		} else if (YoN == 'N' || YoN == 'n') {
			confirmation = FALSE;
		} else {
			printf("Por favor responda Y (si) o N (no)\n.");
			exit = FALSE;
		}
	}

	return confirmation;
}

/*Function that saves a given client structure in csv format at route\\clients.txt.
This function doesn't update the debts file on its own.*/
void saveClient(Client client, char route[150]) {
	char *name = malloc(41*sizeof(char));
	char *line = malloc(250*sizeof(char));
	char *fullRoute = malloc(172*sizeof(char));
	char current;
	FILE *clientFile;
	FILE *auxClientFile;
	int pos = 0;
	int inserted = FALSE;

	memset(name, '\0', 41*sizeof(char));
	memset(line, '\0', 250*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
	clientFile = fopen(fullRoute, "r");
	memset(fullRoute, '\0', 172*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clientsAuxiliary.txt");
	auxClientFile = fopen(fullRoute, "w");
	current = fgetc(clientFile);

	while(1) {
		while (current != '\n' && current != EOF) {

			if (current == ';') {
				pos++;
			}

			if (pos == 0) {
				appendChar(name, current);
			}

			appendChar(line, current);
			current = fgetc(clientFile);
		}

		if (current != EOF && namecmp(name, client->name, 41, 41) >= 0 && !inserted) {	//Found place for the client
			writeClientInFile(client, auxClientFile);
			inserted = TRUE;
		}

		if (current == EOF) {
			if (!inserted) {
				writeClientInFile(client, auxClientFile);
			}
			break;
		}

		appendChar(line, '\n');
		current = fgetc(clientFile);
		fputs(line, auxClientFile);
		pos = 0;
		memset(name, '\0', 41*sizeof(char));
		memset(line, '\0', 250*sizeof(char));
	}

	fclose(clientFile);
	fclose(auxClientFile);
	auxClientFile = fopen(fullRoute, "r");
	memset(fullRoute, '\0', 172*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
	clientFile = fopen(fullRoute, "w");
	current = fgetc(auxClientFile);

	while (current != EOF) {
		fputc(current, clientFile);
		current = fgetc(auxClientFile);
	}

	fclose(clientFile);
	fclose(auxClientFile);
	free(name);
	free(line);
	free(fullRoute);
	name = NULL;
	line = NULL;
	fullRoute = NULL;
}

/*Auxiliary function which writes down data of a given client structure into clientFile.
Employed by saveClient when looking for the position to insert a new client.*/
void writeClientInFile(Client client, FILE *clientFile) {
	char *line = malloc(250*sizeof(char));
	char *tempBuffer = malloc(9*sizeof(char));
	struct NodeD *curNode = client->debts;
	sprintf(line, "%s;%s;%d;%d;%d;", client->name, client->surname, client->dni, client->age, client->dniRecommendedBy);

	while (curNode != NULL) {
		sprintf(tempBuffer, "%d;", curNode->value->id);
		strcat(line, tempBuffer);
		curNode = curNode->next;
	}

	fputs(line, clientFile);
	fputc('\n', clientFile);
	line = NULL;
	tempBuffer = NULL;
	free(line);
	free(tempBuffer);
}

/*Function which appends a debt structure's data in csv format to file at route\\debts.txt*/
void saveDebt(Debt debt,char route[150]) {
	char *fullRoute = malloc(162*sizeof(char));
	char *line = malloc(250*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\debts.txt");
	FILE *debtFile = fopen(fullRoute, "a");

	sprintf(line, "%d;%d;%s;%s;%d;%d;%d;%d;\n", debt->id, debt->dniDebtor, debt->emmissionDate, debt->expirationDate, debt->numberOfFees, debt->feesRemaining, debt->amount, debt->remainingAmount);
	fputs(line, debtFile);
	line = NULL;
	fullRoute = NULL;
	free(line);
	free(fullRoute);
	fclose(debtFile);
}

/*Recibe el id de un credito por parametro y lo elimina
 * return 1 si lo encontro*/
int deleteDebt(int id,char route[150]){

	char *fullRoute = malloc(172*sizeof(char));
	char *fullRouteAuxiliary = malloc(172*sizeof(char));
	char current;
	char idDebt[9];
	memset(idDebt,'\0', sizeof(idDebt));
	FILE *debtFile;
	FILE *debtFileAuxiliary;
	int found = 0;
	int count = 0;
	int index = 0;
	int pos = 0;
	int line = 0;

	memset(fullRoute, '\0', 172*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\debts.txt");
	debtFile = fopen(fullRoute, "r");
	memset(fullRouteAuxiliary, '\0', 172*sizeof(char));
	strcpy(fullRouteAuxiliary, route);
	strcat(fullRouteAuxiliary, "\\debtsAuxiliary.txt");
	debtFileAuxiliary = fopen(fullRouteAuxiliary, "w");
	current = fgetc(debtFile);

	while(found == 0 && current != EOF){
			while(current != ';'){
				switch(count){
				case 0:
					idDebt[index] = current;
					break;
				default:
					break;
				}
				index++;
				current = fgetc(debtFile);
			}
			if(strlen(idDebt) != 0){
				if(id != strtoint(idDebt, 7)){
					memset(idDebt,'\0', sizeof(idDebt));
					skipLine(debtFile);
					line++;
					current = fgetc(debtFile);
					count = 0;
					index = 0;
				}else{
					found = 1;
				}
			}else{
				current = fgetc(debtFile);
				index = 0;
				count++;
			}
			continue;
		}
	if(found == 0){
		return found;
		}


	fclose(debtFile);
	debtFile = fopen(fullRoute, "r");
	current = fgetc(debtFile);

	while(current != EOF){
		if(pos!= line){
			while(current != '\n'){
				fputc(current,debtFileAuxiliary);
				current = fgetc(debtFile);
			}
			fputc(current,debtFileAuxiliary);
			current = fgetc(debtFile);
		}else{
			skipLine(debtFile);
			current = fgetc(debtFile);
		}
		pos++;
	}

	fclose(debtFile);
	fclose(debtFileAuxiliary);

	debtFile = fopen(fullRoute, "w");
	debtFileAuxiliary = fopen(fullRouteAuxiliary, "r");
	current = fgetc(debtFileAuxiliary);
	while(current != EOF){
		fputc(current, debtFile);
		current = fgetc(debtFileAuxiliary);
	}
	fclose(debtFile);
	fclose(debtFileAuxiliary);

	return found;
}

/*Function which edits the newAmount and newFees fields of a credit stored in
CSV format at route\\debts.txt.*/
void editDebt(int id, int newAmount, int newFees, char route[150]) {
	char *strId = malloc(9*sizeof(char));
	char *line = malloc(250*sizeof(char));
	char *fullRoute = malloc(172*sizeof(char));
	char current;
	FILE *debtFile;
	FILE *auxFile;
	int pos = 0;

	cleanStrings(strId, 9*sizeof(char), line, 250*sizeof(char), fullRoute, 172*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\debts.txt");
	debtFile = fopen(fullRoute, "r");
	memset(fullRoute, '\0', 172*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clientsAuxiliary.txt");	//name is irrelevant, it's just a temporary buffer file
	auxFile = fopen(fullRoute, "w");
	current = fgetc(debtFile);

	while(1) {
		while (current != '\n' && current != EOF) {

			if (current == ';') {
				pos++;
			}

			if (pos == 0) {
				appendChar(strId, current);
			}

			appendChar(line, current);
			if (id == strtoint(strId, strlen(strId)-1)) {	//Found credit to modify
				modifyDebt(newAmount, newFees, debtFile, line);
				break;
			}
			current = fgetc(debtFile);
		}

		if (current == EOF) {
			break;
		}

		current = fgetc(debtFile);
		appendChar(line, '\n');
		fputs(line, auxFile);
		pos = 0;
		memset(strId, '\0', 41*sizeof(char));
		memset(line, '\0', 250*sizeof(char));
	}

	fclose(debtFile);
	fclose(auxFile);
	auxFile = fopen(fullRoute, "r");
	memset(fullRoute, '\0', 172*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\debts.txt");
	debtFile = fopen(fullRoute, "w");
	current = fgetc(auxFile);
	printf("%c", current);
	while (current != EOF) {
		fputc(current, debtFile);
		current = fgetc(auxFile);
	}

	fclose(debtFile);
	fclose(auxFile);
	free(strId);
	free(line);
	free(fullRoute);
	strId = NULL;
	line = NULL;
	fullRoute = NULL;
}

void modifyDebt(int newAmount, int newFees, FILE *debtFile, char line[250]) {
	char current = fgetc(debtFile);
	char temp_int[9];
	int pos = 0;
	memset(temp_int, '\0', sizeof(temp_int));

	while (current != '\n') {
		if (current == ';') {
			pos++;
		}

		switch(pos) {
		case 5:
			sprintf(temp_int, ";%d", newFees);
			strcat(line, temp_int);
			memset(temp_int, '\0', sizeof(temp_int));
			current = nextSemiColon(debtFile);
			break;
		case 7:
			sprintf(temp_int, ";%d", newAmount);
			strcat(line, temp_int);
			current = nextSemiColon(debtFile);
		default:
			appendChar(line, current);
			current = fgetc(debtFile);
			break;
		}
	}
}

char nextSemiColon(FILE *file) {
	char current = fgetc(file);
	while (current != ';') {
		current = fgetc(file);
	}
	return current;
}

/*Agrega id de credito a cliente en el archivo. Recibe por parametro el dni del cliente al que se le va a
 * agregar el id de credito.
 * return 1 si lo encuentra*/
int addIdCreditToClient(int dni, int id, char route[150]){

	char *fullRoute = malloc(172*sizeof(char));
	char *fullRouteAuxiliary = malloc(172*sizeof(char));
	char current;
	char idClient[9];
	char dniClient[9];
	memset(dniClient,'\0', sizeof(dniClient));
	memset(idClient,'\0', sizeof(dniClient));
	FILE *clientFile;
	FILE *clientFileAuxiliary;
	int found = 0;
	int count = 0;
	int index = 0;
	int pos = 0;
	int line = 0;

	memset(fullRoute, '\0', 172*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
	clientFile = fopen(fullRoute, "r");
	memset(fullRouteAuxiliary, '\0', 172*sizeof(char));
	strcpy(fullRouteAuxiliary, route);
	strcat(fullRouteAuxiliary, "\\clientsAuxiliary.txt");
	clientFileAuxiliary = fopen(fullRouteAuxiliary, "w");
	current = fgetc(clientFile);
	while(found == 0 && current != EOF){
			while(current != ';'){
				switch(count){
				case 2:
					dniClient[index] = current;
					break;
				default:
					break;
				}
				index++;
				current = fgetc(clientFile);
				continue;
			}
			if(strlen(dniClient) != 0){
				if(dni != strtoint(dniClient, 7)){
					memset(dniClient,'\0', sizeof(dniClient));
					skipLine(clientFile);
					line++;
					current = fgetc(clientFile);
					count = 0;
					index = 0;
				}else{
					found = 1;
				}
			}else{
				current = fgetc(clientFile);
				index = 0;
				count++;
			}
			continue;
		}
	if(found == 0){
		return found;
		}
	fclose(clientFile);
		clientFile = fopen(fullRoute, "r");
		current = fgetc(clientFile);
		count = 0;
		while(current != EOF){
			if(pos != line){
				while(current != '\n'){
					fputc(current, clientFileAuxiliary);
					current = fgetc(clientFile);
				}
				fputc(current, clientFileAuxiliary);
				current = fgetc(clientFile);
			}else{
				while(current != '\n'){
					fputc(current, clientFileAuxiliary);
					current = fgetc(clientFile);
				}
				sprintf(idClient, "%d", id);
				fputs(idClient,clientFileAuxiliary);
				fputc(';',clientFileAuxiliary);
				fputc(current,clientFileAuxiliary);
				current = fgetc(clientFile);
			}
			pos++;
		}
	fclose(clientFile);
	fclose(clientFileAuxiliary);
	clientFile = fopen(fullRoute, "w");
	clientFileAuxiliary = fopen(fullRouteAuxiliary, "r");
	current = fgetc(clientFileAuxiliary);
	while(current != EOF){
		fputc(current, clientFile);
		current = fgetc(clientFileAuxiliary);
		continue;
	}

	fclose(clientFile);
	fclose(clientFileAuxiliary);
	fullRoute = NULL;
	fullRouteAuxiliary = NULL;

	return found;
}
/*Recibe por parametro el dni de un cliente y el id de un credito del mismo, el cual sera borrado del
 * archivo de clientes
 * return 1 si lo encuentra*/
int deleteIdInClientFile(int dni,int id, char route[150]){

	char *fullRoute = malloc(172*sizeof(char));
	char *fullRouteAuxiliary = malloc(172*sizeof(char));
	char current;
	char idClient[9];
	char dniClient[9];
	memset(dniClient,'\0', sizeof(dniClient));
	memset(idClient,'\0', sizeof(dniClient));
	FILE *clientFile;
	FILE *clientFileAuxiliary;
	int found = 0;
	int count = 0;
	int index = 0;
	int pos = 0;
	int line = 0;

	memset(fullRoute, '\0', 172*sizeof(char));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
	clientFile = fopen(fullRoute, "r");
	memset(fullRouteAuxiliary, '\0', 172*sizeof(char));
	strcpy(fullRouteAuxiliary, route);
	strcat(fullRouteAuxiliary, "\\clientsAuxiliary.txt");
	clientFileAuxiliary = fopen(fullRouteAuxiliary, "w");
	current = fgetc(clientFile);
	while(found == 0 && current != EOF){
		while(current != ';'){
			switch(count){
			case 2:
				dniClient[index] = current;
				break;
			default:
				break;
			}
			index++;
			current = fgetc(clientFile);
			continue;
		}
		if(strlen(dniClient) != 0){
			if(dni != strtoint(dniClient, 7)){
				memset(dniClient,'\0', sizeof(dniClient));
				skipLine(clientFile);
				line++;
				current = fgetc(clientFile);
				count = 0;
				index = 0;
			}else{
				found = 1;
			}
		}else{
			current = fgetc(clientFile);
			index = 0;
			count++;
		}
		continue;
	}
	if(found == 0){
		return found;
		}
	fclose(clientFile);
	clientFile = fopen(fullRoute, "r");
	current = fgetc(clientFile);
	count = 0;
	while(current != EOF){
		if(pos != line){
			while(current != '\n'){
				fputc(current, clientFileAuxiliary);
				current = fgetc(clientFile);
			}
			fputc(current, clientFileAuxiliary);
			current = fgetc(clientFile);
		}else{
			while(current != '\n'){
				while(current != ';'){
					switch(count){
					case 5:
						idClient[index] = current;
						break;
					case 6:
						idClient[index] = current;
						break;
					case 7:
						idClient[index] = current;
						break;
					default:
						index = 0;
						fputc(current, clientFileAuxiliary);
						break;
					}
					index++;
					current = fgetc(clientFile);
				}
				if(strlen(idClient) != 0){
					if(strtoint(idClient,7) != id){
						fputs(idClient, clientFileAuxiliary);
						memset(idClient,'\0', sizeof(idClient));
					}
				}
			if(strtoint(idClient,7) != id){
				fputc(current,	 clientFileAuxiliary);
			}
			memset(idClient,'\0', sizeof(idClient));
			count++;
			index = 0;
			current = fgetc(clientFile);
			}
		}
		pos++;
	}
	fclose(clientFile);
	fclose(clientFileAuxiliary);
	clientFile = fopen(fullRoute, "w");
	clientFileAuxiliary = fopen(fullRouteAuxiliary, "r");
	current = fgetc(clientFileAuxiliary);
	while(current != EOF){
		fputc(current, clientFile);
		current = fgetc(clientFileAuxiliary);
		continue;
	}

	fclose(clientFile);
	fclose(clientFileAuxiliary);
	fullRoute = NULL;
	fullRouteAuxiliary = NULL;

	return found;
}



