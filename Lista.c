#include "Lista.h"
#include "FileManager.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void insertClient(ListC *list, Client value) {
    struct NodeC *node = malloc(sizeof(struct NodeC));
    node->next = NULL;
    node->value = value;
    if ((*list) == NULL) {
        (*list) = node;
		return;
    }
    struct NodeC *pos = *list;

    while (pos->next != NULL) {
        pos = pos->next;
    }

    pos->next = node;
}

void removeClient(ListC *list, Client value) {
    struct NodeC *pos = *list;
    if (list == NULL) {
        return;
    } else if ((*list)->value->dni == value->dni) {
        (*list) = (*list)->next;
    } else {
        while (pos->next != NULL) {
            if (pos->next->value->dni == value->dni) {
                pos->next = pos->next->next;
                break;
            } else {
                pos = pos->next;
            }
        }
    }
}

void printListC(ListC list, char route[150]) {
	struct NodeC *pos = list;

	if (list == NULL) {
		printf("No hay clientes para listar.\n");
	}

	while(pos != NULL) {
		printClient(pos->value, route);
		pos = pos->next;
	}
}

void printClient(Client client, char route[150]) {
	Client recommender;
	char fullName[84];
	int numDebts = 0;
	memset(fullName, '\0', sizeof(fullName));
	printf("-------------------------------------------------------------------------------------------------------------------------");
	printf("\n\nNombre: %s", client->name);
	printf("\nApellido: %s", client->surname);
	printf("\nDNI: %d", client->dni);
	printf("\nEdad: %d", client->age);
	if (client->dniRecommendedBy != 0) {
		recommender = loadClientDNI(client->dniRecommendedBy, route);
		strcat(fullName, recommender->name);
		strcat(fullName, " ");
		strcat(fullName, recommender->surname);
	} else {
		strcpy(fullName, "no hay recomendador");
	}
	printf("\nRecomendador: %s", fullName);
	numDebts = countDebts(client->debts);
	printf("\nCreditos pendientes: %d\n\n", numDebts);
}

void insertDebt(ListD *list, Debt value) {
    struct NodeD *node = malloc(sizeof(struct NodeD));
    node->next = NULL;
    node->value = value;
    if ((*list) == NULL) {
        (*list) = node;
		return;
    }
    struct NodeD *pos = *list;

    while (pos->next != NULL) {
        pos = pos->next;
    }

    pos->next = node;
}

void removeDebt(ListD *list, Debt value) {
    struct NodeD *pos = *list;
    if (list == NULL) {
        return;
    } else if ((*list)->value->id == value->id) {
        (*list) = (*list)->next;
    } else {
        while (pos->next != NULL) {
            if (pos->next->value->id == value->id) {
                pos->next = pos->next->next;
                break;
            } else {
                pos = pos->next;
            }
        }
    }
}

void printListD(ListD list) {
	struct NodeD *pos = list;

	if (list == NULL) {
		printf("No hay creditos para listar.\n");
	}

	while(pos != NULL) {
		printDebt(pos->value);
		pos = pos->next;
	}
}

void printDebt(Debt debt) {
	printf("ID: %d\n", debt->id);
	printf("Fecha de emision: %s\n", debt->emmissionDate);
	printf("Fecha de expiracion: %s\n", debt->expirationDate);
	printf("Cuotas totales: %d\n", debt->numberOfFees);
	printf("Cuotas restantes: %d\n", debt->feesRemaining);
	printf("Monto pedido: %d\n", debt->amount);
	printf("Monto debido: %d\n\n", debt->remainingAmount);
}

int countDebts(ListD list) {
    struct NodeD *curNode = list;
    int counter = 0;

    while (curNode != NULL) {
        counter++;
        curNode = curNode->next;
    }
    return counter;
}
