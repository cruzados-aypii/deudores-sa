#ifndef LISTA_H
#define LISTA_H

typedef struct Debt {
	int id;
    int dniDebtor;
    char emmissionDate[11];
    char expirationDate[11];
	int numberOfFees;
	int feesRemaining;
    int amount;
    int remainingAmount;
} *Debt;

typedef struct NodeD {
    Debt value;
    struct NodeD *next;
} *ListD;

typedef struct Client {
    char name[41];
    char surname[43];
    int age;
    int dni;
    int dniRecommendedBy;
    ListD debts;
} *Client;

typedef struct NodeC {
    Client value;
    struct NodeC *next;
} *ListC;

void insertClient(ListC *list, Client value);
void removeClient(ListC *list, Client value);
void printListC(ListC list, char route[150]);
void printClient(Client client, char route[150]);

void insertDebt(ListD *list, Debt value);
void removeDebt(ListD *list, Debt value);
void printListD(ListD list);
void printDebt(Debt debt);
int countDebts(ListD list);

#endif
