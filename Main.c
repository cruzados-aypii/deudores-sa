#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Lista.h"
#include "FileManager.h"
#include "RandomExample.h"

int validarDia30() {
	int salirAux;
	int EsNumero;
	int dia;

	do{
		salirAux = TRUE;
		printf("Ingrese el dia\n");
		EsNumero = scanf("%d",&dia);
		while ((getchar()) != '\n');
		if(!EsNumero){
			printf("No ingreso un numero \n");
			printf("Por favor ingrese un numero \n");
			salirAux = FALSE;
		}else if (dia < 1 || dia > 30) {
			printf("El mes consta de 30 dias\n");
			salirAux = FALSE;
		}
	}while(!salirAux);

	return dia;
}

int validarDia31() {
	int EsNumero;
	int salirAux;
	int dia;

	do{
		salirAux = TRUE;
		printf("Ingrese el dia\n");
		EsNumero = scanf("%d",&dia);
		while ((getchar()) != '\n');
		if (!EsNumero) {
			printf("No ingreso un numero \n");
			printf("Por favor ingrese un numero \n");
			salirAux = FALSE;
		} else if (dia < 1 || dia > 31) {
			printf("El mes consta de 31 dias\n");
			salirAux = FALSE;
		}
	}while(!salirAux);

	return dia;
}

void validarFecha(char fechaEmision[11],char fechaVencimiento[11],int NCuotas){
	int dia;
	int mes;
	int anio;
	int mesVencimiento;
	int anioVencimiento;
	int salirAux;
	int EsNumero;

	do{
		salirAux = TRUE;
		printf("Ingrese el anio\n");
		EsNumero = scanf("%d",&anio);
		while ((getchar()) != '\n');
		if (!EsNumero){
			printf("No ingreso un numero \n");
			printf("Por favor ingrese un numero \n");
			salirAux = FALSE;
		} else if(anio <2000){
			printf(" el anio minimo es el anio 2000\n");
			salirAux = FALSE;
		}
	}while(!salirAux);

	do{
		salirAux = TRUE;
		printf("Ingrese el mes\n");
		EsNumero = scanf("%d",&mes);
		while ((getchar()) != '\n');
		if (!EsNumero){
			printf("No ingreso un numero \n");
			printf("Por favor ingrese un numero \n");
			salirAux = FALSE;
		}else if(mes<0 || mes>12){
			printf("Los meses van de 1 a 12\n");
			salirAux = FALSE;
		}else{
			switch(mes){
				case 1:	
						dia = validarDia31(dia);
						break;
				case 2:
						do{
							salirAux=TRUE;
							printf("Ingrese el dia\n");
							EsNumero=scanf("%d",&dia);
							while ((getchar()) != '\n');
							if(!EsNumero){
								printf("No ingreso un numero \n");
								printf("Por favor ingrese un numero \n");
								salirAux=FALSE;
							}else if(dia<0 || dia>28){
								printf("El mes consta de 28 dias\n");
								salirAux=FALSE;
							}
						}while(!salirAux);
						break;
				case 3:
						dia = validarDia31();
						break;
				case 4:	
						dia = validarDia30();
						break;
				case 5:
						dia = validarDia31();
						break;
				case 6:
						dia = validarDia30();
						break;
				case 7:
						dia = validarDia31();
						break;
				case 8: 
						dia = validarDia31();
						break;
				case 9: 
						dia = validarDia30();
						break;
				case 10: 
						dia = validarDia31();
						break;
				case 11: 
						dia = validarDia30();
						break;
				case 12: 
						dia = validarDia31();
						break;
				}
		}
	}while(!salirAux);

	mesVencimiento = mes + NCuotas;
	anioVencimiento = anio;
	while(mesVencimiento> 12){
		mesVencimiento = mesVencimiento-12;
		anioVencimiento = anioVencimiento+1;
	}
	sprintf(fechaEmision, "%d-%d-%d", dia, mes,anio);
	sprintf(fechaVencimiento, "%d-%d-%d", dia, mesVencimiento,anioVencimiento);
}

void imprimirClientesPorRangoDeEdad(char rutaRepositorio[150]){
	int min;
	int max;
	int EsNumero;
	int salirAux;
	do{
		printf("Ingrese la edad minima \n");
		salirAux=TRUE;
		EsNumero=scanf("%d",&min);
		while ((getchar()) != '\n');
		if (!EsNumero){
			printf("No ingreso un numero \n");
			printf("Por favor ingrese un numero \n");
			salirAux=FALSE;
		}else if(min < 18){
			printf("La minima edad de los clientes es 18\n");
			salirAux=FALSE;
		}
	}while(!salirAux);
	do{
		printf("Ingrese la edad maxima \n");
		salirAux=TRUE;
		EsNumero=scanf("%d",&max);
		while ((getchar()) != '\n');
		if (!EsNumero){
			printf("No ingreso un numero \n");
			printf("Por favor ingrese un numero \n");
			salirAux=FALSE;
		}else if(min>max){
			printf("La edad maxima tiene que ser mayor que la edad minima\n");
			salirAux=FALSE;
		}
	}while(!salirAux);
	loadAndPrintRangeOfClientsAge(min, max, rutaRepositorio);
}

void pagarCuota(Client cliente,char rutaRepositorio[150]){
	ListD pCredito= cliente->debts;
	int salirAux;
	char YoN;

	if (pCredito == NULL) {
		printf("El cliente no tiene creditos pendientes.\n");
		return;
	}

	do{
		salirAux=TRUE;
		printf("Detalles de credito:\n");
		printf("Fecha de emicion: %s\n", pCredito->value->emmissionDate);
		printf("Fecha de vencimiento: %s\n",pCredito->value->expirationDate);
		printf("Cuotas restantes: %d\n", pCredito->value->feesRemaining);
		printf("Deuda restante: %d\n", pCredito->value->remainingAmount);
		printf("Desea pagar una cuota de este credito? Y/N\n");
		scanf("%c",&YoN);
		while ((getchar()) != '\n');

		if (YoN == 'y') {
			pCredito->value->feesRemaining = pCredito->value->feesRemaining-1;
			if (pCredito->value->feesRemaining == 0) {
				deleteDebt(pCredito->value->id, rutaRepositorio);
				deleteIdInClientFile(cliente->dni, pCredito->value->id, rutaRepositorio);
				printf("\nEl credito fue pagado en su totalidad\n");
			} else {
				pCredito->value->remainingAmount = pCredito->value->remainingAmount-(pCredito->value->amount / pCredito->value->numberOfFees);
				editDebt(pCredito->value->id,pCredito->value->remainingAmount,pCredito->value->feesRemaining, rutaRepositorio);
				printf("La cuota del credito fue pagada\n");
				printf("Resta pagar: \n");
				printf("Cuotas restantes: %d\n", pCredito->value->feesRemaining);
				printf("Deuda restante: %d\n", pCredito->value->remainingAmount);
			}
		} else if (YoN == 'n') {
			pCredito = pCredito->next;
			salirAux = FALSE;

		} else {
			printf("\nOpcion invalida \n");
			salirAux=FALSE;
		}
	}while(!salirAux && pCredito!=NULL);
}

int checkFileExistence(char route[150]) {
	char fullRoute[162];
	FILE *clientFile;
	memset(fullRoute, '\0', sizeof(fullRoute));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
	clientFile = fopen(fullRoute, "r");
	if (clientFile == NULL) {
		printf("\nEl archivo indicado por la ruta dada no existe\n");
		return FALSE;
	} else {
		fclose(clientFile);
		return TRUE;
	}
}

int overwriteFiles(char route[150]) {
	char fullRoute[162];
	FILE *file;
	memset(fullRoute, '\0', sizeof(fullRoute));
	strcpy(fullRoute, route);
	strcat(fullRoute, "\\clients.txt");
	file = fopen(fullRoute, "w");
	if (file == NULL) {
		printf("\nEl directorio indicado por la ruta dada no existe\n");
		return FALSE;
	} else {
		fclose(file);
		memset(fullRoute, '\0', sizeof(fullRoute));
		strcpy(fullRoute, route);
		strcat(fullRoute, "\\debts.txt");
		file = fopen(fullRoute, "w");
		fclose(file);
		return TRUE;
	}
}

int validarNombre(char nombre[41]){
	int salirAux=FALSE;
	int i=0;
	while(nombre[i]!= '\0' && !salirAux){
		
		if(((nombre[i]>64 && nombre[i]<91) ||(nombre[i]>96 && nombre[i]<123) || nombre[i]==32)){
			if (i==0){
				if (nombre[0] >= 'a') {
        			nombre[0] -= 32;
				}
			}else{
				if(nombre[i-1]== 32){
					if (nombre[i] >= 'a') {
        				nombre[i] -= 32;
					}
				}
			}
			i=i+1;
		}else{
			printf("El nombre no puede tener un caracter especial\n");
			salirAux=TRUE;
		}
	}
	return !salirAux;
}

int validarApellido(char apellido[43]){
	int salirAux=FALSE;
	int i=0;
	while(apellido[i]!= '\0' && !salirAux){
		
		if(((apellido[i]>64 && apellido[i]<91) ||(apellido[i]>96 && apellido[i]<123) || apellido[i]==32) ){
			if (i==0){
				if (apellido[0] >= 'a') {
        			apellido[0] -= 32;
				}
			}else{
				if(apellido[i-1]== 32){
					if (apellido[i] >= 'a') {
        				apellido[i] -= 32;
					}
				}
			}
			i=i+1;
		}else{
			printf("El apellido no puede tener un caracter especial\n");
			salirAux=TRUE;
		}
	}
	return !salirAux;
}

int main(){
	Client cliente;
	char rutaRepositorio[150];
    int opcion;
    int EsNumero;
    char YoN;
    char nombre[41];
    char apellido[43];
    int dni;
    int edad;
    int recomendado;
    int IDCredito;
    int NCuotas;
    int monto;
    char fechaEmision[11];
    char fechaVencimiento[11];
	int salir = FALSE;
	int salirAux;

	do{
		printf("\nDesea crear una nueva sesion? Y-N \n");
		scanf("%s", &YoN);
		while ((getchar()) != '\n');
		if (YoN == 'n' || YoN == 'N') {
			printf("\nInserte la ruta del directorio donde se almacenan los archivos:\n");
			scanf("%[^\n]s ", rutaRepositorio);
			while ((getchar()) != '\n');
			salirAux = checkFileExistence(rutaRepositorio);
		} else if (YoN == 'y' || YoN == 'Y') {
			printf("\nInserte la ruta del directorio donde se almacenaran los archivos:\n");
			scanf("%[^\n]s ", rutaRepositorio);
			while ((getchar()) != '\n');
			salirAux = overwriteFiles(rutaRepositorio);
		} else {
			printf("\nOpcion invalida\n");
			salirAux = FALSE;
		}
	}while(!salirAux);
	
    while(!salir) {
		do{
			printf("\n");
			printf("Escriba solamente el numero correspondiente a la opcion que desea elegir: \n");
			printf("1: Dar de alta un nuevo cliente.\n");
			printf("2: Dar de alta un credito a un cliente.\n");
			printf("3: Pagar cuota de credito de un cliente.\n");
			printf("4: Buscar un cliente por...\n");
			printf("5: Buscar clientes por un rango de edad.\n");
			printf("6: Imprimir todos los clientes ordenados alfabeticamente.\n");
			printf("7: Imprimir clientes alfabeticamente en paginas (imprime de a 100 clientes).\n");
			printf("8: Generar un ejemplo aleatorio (los nombres de los clientes seran combinaciones aleatorias de letras).\n");
			printf("9: Salir.\n");
			EsNumero=scanf("%d",&opcion);
			while ((getchar()) != '\n');
			if (!EsNumero){
				printf("No ha ingresado un numero.\n");
				printf("Por favor ingrese un numero.\n");
			}
		}while(!EsNumero);
		switch(opcion){
		case 1:
			do{
				printf("Ingrese el nombre del cliente\n");
				scanf("%[^\n]s ",nombre);
				while ((getchar()) != '\n');
				salirAux=validarNombre(nombre);
			}while(!salirAux);
			do{
				printf("Ingrese el apellido del cliente\n");
				scanf("%[^\n]s ",apellido);
				while ((getchar()) != '\n');
				salirAux=validarApellido(apellido);
			}while(!salirAux);
			do{
				salirAux=TRUE;
				printf("Ingrese el dni del cliente\n");
				EsNumero=scanf("%d",&dni);
				while ((getchar()) != '\n');
				if (!EsNumero){
					printf("No ingreso un numero \n");
					printf("Por favor ingrese un numero \n");
					salirAux=FALSE;
				} else if(dni <0){
					printf("El dni no puede ser un nuemero negativo\n");
					salirAux=FALSE;
				}else if(dni<=9999999 && dni>= 100000000){
					printf("El dni tienen que tener 8 caracteres\n");
					salirAux=FALSE;
				}
			}while(!salirAux);
			do{
				salirAux=TRUE;
				printf("Ingrese la edad del cliente\n");
				scanf("%d",&edad);
				while ((getchar()) != '\n');
				if (!EsNumero){
					printf("No ingreso un numero \n");
					printf("Por favor ingrese un numero \n");
					salirAux=FALSE;
				} else if(edad <18){
					printf("La edad del cliente tiene que 18 anios o mayor\n");
					salirAux=FALSE;
				}
			}while(!salirAux);
			do{
				salirAux=TRUE;
				printf("Es recomendado Y-N\n");
				scanf("%s", &YoN);
				while ((getchar()) != '\n');
				if(YoN == 'y'){
					printf("Ingrese el dni del cliente que lo recomendo\n");
					scanf("%d",&recomendado);
					while ((getchar()) != '\n');
					if (!EsNumero){
						printf("No ingreso un numero \n");
						printf("Por favor ingrese un numero \n");
						salirAux=FALSE;
					}
					cliente=loadClientDNI(recomendado,rutaRepositorio);
					if(cliente!=NULL){
						
					}else{
						printf ("El dni ingresado no pertenece a ningun cliente\n");
						salirAux=FALSE;
					}
				}else if(YoN == 'n'){
					recomendado = 0;
				}else{
					printf("Opcion invalida\n");
					salirAux=FALSE;
				}
			}while(!salirAux);
			printf("Nombre: %s\n",nombre);
			printf("Apellido: %s\n", apellido);
			printf("Dni: %d\n", dni);
			printf("Edad: %d\n", edad);
			if(!recomendado){
				printf("No tiene recomendacion\n");
			}else{
				printf("Recomendado por: %d\n", recomendado);
			}
			do{
				salirAux=TRUE;
				printf("Desea crear este cliente? Y-N\n");
				scanf("%s",&YoN);
				while ((getchar()) != '\n');
				if(YoN == 'y'){
					cliente=loadClientDNI(dni,rutaRepositorio);
					if(cliente!=NULL){
						printf("El cliente ya existe\n");
					}else{	
						saveClient(initializeClient(nombre,apellido,dni,edad,recomendado, NULL),rutaRepositorio);
						printf("El cliente fue creado con exito\n");
					}
				}else if (YoN == 'n'){
		
				}else{
					printf("opcion invalida\n");
					salirAux=FALSE;
				}
			}while(!salirAux);
			break;
		case 2:
			do{
				printf("Ingrese el dni del cliente\n");
				EsNumero=scanf("%d",&dni);
				while ((getchar()) != '\n');
				if (!EsNumero){
					printf("No ingreso un numero \n");
					printf("Por favor ingrese un numero \n");
				}
			}while(!EsNumero);
			cliente=loadClientDNI(dni,rutaRepositorio);
			if(cliente){
				printf("El cliente fue encontrado\n");
				do{
					salirAux=TRUE;
					printf("Ingrese el numero de cuotas\n");
					printf("El numero de cuotas puede ser de 6,12,24,36\n");
					EsNumero=scanf("%d",&NCuotas);
					while ((getchar()) != '\n');
					if (!EsNumero){
						salirAux=FALSE;
						printf("No ingreso un numero \n");
						printf("Por favor ingrese un numero \n");
					}else{
						switch(NCuotas){
						case 6:
							break;
						case 12:
							break;
						case 24:
							break;
						case 36:
							break;
						default:
							printf("El numero de cuotas no es correcto\n");
							salirAux=FALSE;
							break;
						}
					}
				}while(!salirAux);
				do{
					salirAux=TRUE;
					printf("Ingrese el monto pedido\n");
					EsNumero=scanf("%d",&monto);
					while ((getchar()) != '\n');
					if (!EsNumero){
						printf("No ingreso un numero \n");
						printf("Por favor ingrese un numero \n");
						salirAux=FALSE;
					}else if(monto<1){
						printf("El monto tiene que ser mayor que 0\n");
						salirAux=FALSE;
					}
				}while(!salirAux);
				validarFecha(fechaEmision,fechaVencimiento,NCuotas);
				do{
					salirAux=TRUE;
					printf("Monto: %d\n", monto);
					printf("Cantidad de cuotas: %d\n", NCuotas);
					printf("Fecha de emision: %s\n", fechaEmision);
					printf("Fecha de vencimiento: %s\n", fechaVencimiento);
					printf("Desea crear este credito? Y-N\n");
					scanf("%s", &YoN);
					while ((getchar()) != '\n');
					if(YoN == 'y'){
						do{
							salirAux=TRUE;
							IDCredito = randInt(10000000, 99999999);
							if(loadDebt(IDCredito,rutaRepositorio)){
								salirAux=FALSE;
							}
						}while(!salirAux);
						addIdCreditToClient(dni,IDCredito, rutaRepositorio);
						saveDebt(initializeDebt(IDCredito,dni,fechaEmision,fechaVencimiento,NCuotas,NCuotas,monto,monto),rutaRepositorio);
					}else if (YoN == 'n'){

					}else{
						printf("opcion invalida\n");
						salirAux=FALSE;
					}
				}while(!salirAux);
			}else{
				printf("El cliente ingresado no existe\n");
			}
		break;
		case 3:
			do{
				salirAux=TRUE;
				printf("Ingrese el dni del cliente\n");
				EsNumero=scanf("%d",&dni);
				while ((getchar()) != '\n');
				if (!EsNumero){
					printf("No ha ingresado un numero, por favor ingrese un numero\n");
					salirAux=FALSE;
				} else if(dni <0){
					printf("El dni no puede ser un nuemero negativo\n");
					salirAux=FALSE;
				}else if(dni<=9999999 && dni>= 100000000){
					printf("El dni debe tener 8 caracteres\n");
					salirAux=FALSE;
				}
			}while(!salirAux);
			cliente= loadClientDNI(dni,rutaRepositorio);
			if(cliente){
				pagarCuota(cliente,rutaRepositorio);
			}else{
				printf("El cliente ingresado no existe\n");
			}
			break;
		case 4:
			do{			
				salirAux=TRUE;	
				do{
					printf("1: Buscar cliente por dni\n");
					printf("2: Buscar cliente por nombre y apellido\n");
					EsNumero=scanf("%d",&opcion);
					while ((getchar()) != '\n');
					if (!EsNumero){
						printf("No ingreso un numero \n");
						printf("Por favor ingrese un numero \n");
					}
				}while(!EsNumero);
				if(opcion == 1){
					salirAux=TRUE;	
					do{
						printf("Ingrese el dni de un cliente\n");
						EsNumero=scanf("%d",&dni);
						while ((getchar()) != '\n');
						if (!EsNumero){
							printf("No ingreso un numero \n");
							printf("Por favor ingrese un numero \n");
							salirAux=FALSE;
						} else if(dni <0){
							printf("El dni no puede ser un nuemero negativo\n");
							salirAux=FALSE;
						}else if(dni<=9999999 && dni>= 100000000){
							printf("El dni tienen que tener 8 caracteres\n");
							salirAux=FALSE;
						}
					}while(!EsNumero);
					cliente= loadClientDNI(dni,rutaRepositorio);
					if(cliente){
						printClient(cliente, rutaRepositorio);
					}else{
						printf("El cliente no existe\n");
					}
				}else if(opcion == 2){
					printf("Ingrese el nombre del cliente\n");
					scanf("%s", nombre);
					printf("Ingrese el apellido del cliente\n");
					scanf("%s", apellido);
					cliente=loadClientName(nombre,apellido,rutaRepositorio);
					if(cliente){
						printClient(cliente, rutaRepositorio);
					}else{
						printf("El cliente no existe\n");
					}
				}else{
					printf("Opcion invalida\n");
					salirAux=FALSE;
				} 
			}while(!salirAux);
			break;
		case 5:
			imprimirClientesPorRangoDeEdad(rutaRepositorio);
			printf("Para mas informacion sobre los creditos de un cliente, busque al cliente correspondiente por su DNI o nombre.\n\n");
			break;
		
		case 6:
			loadAndPrintClients(rutaRepositorio, FALSE);
			break;
		case 7:
			loadAndPrintClients(rutaRepositorio, TRUE);
			break;
		case 8:
			printf("\nIndique la cantidad de clientes a generar, mas de 10 pero menos de 10000 (con numeros grandes puede tomar un tiempo considerable en generarse, pues cada cliente puede tener hasta 3 creditos):\n");
			EsNumero = scanf("%d", &opcion);
			if (EsNumero) {
				generateExample(opcion, rutaRepositorio);
			} else {
				break;
			}
			printf("\nArchivo de ejemplo generado, se recomiendo recorrer el listado de clientes.\n");
			break;
		case 9:
			salir= TRUE;
			break;
		default:
			printf("Opcion invalida \n");
			printf("Elija una opcion valida \n");
			break;
		}
	}
	return 0;
}


